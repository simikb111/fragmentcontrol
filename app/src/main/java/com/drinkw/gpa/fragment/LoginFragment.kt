package com.drinkw.gpa.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.drinkw.gpa.fragment.dummy.DummyContent.staticarray
import com.google.android.material.textfield.MaterialAutoCompleteTextView
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_login.view.*


/**
 * A simple [Fragment] subclass.
 * Use the [LoginFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class LoginFragment : Fragment() {
    private var deptTextView: MaterialAutoCompleteTextView? =
        null;

    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null
    private var array: ArrayList<String>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getString(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
            var a=array
          //1  staticarray=array
                //arguments!!.getStringArrayList(ARG_array)
                // arrayOf("a","b","c","d")
         //   array = a!!.toList()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
       // departmentAdapter()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var v=inflater.inflate(R.layout.fragment_login, container, false)
        deptTextView=v!!.filled_exposed_dropdown
        departmentAdapter()
        return v
    }

    override fun onResume() {
        super.onResume()



    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

     //   deptTextView = view.findViewById(R.id.filled_exposed_dropdown as MaterialAutoCompleteTextView;
        name.setText(mParam1)
        password.setText(mParam2)
        deptTextView=view.filled_exposed_dropdown
      //  departmentAdapter()
        // deptTextView!!.setSelection(0)
        deptTextView!!.setOnItemClickListener { parent, view, position, id ->
            Toast.makeText(activity, staticarray?.get(position)?.toString(),Toast.LENGTH_LONG);
           // departmentAdapter()
        }



        view.button.setOnClickListener {
            val fragmentManager = activity!!.supportFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            val fragment = ItemFragment.newInstance(1,name.text.toString(),password.text.toString())
                fragmentTransaction.replace(R.id.fra, fragment)
                fragmentTransaction.addToBackStack("1")
                fragmentTransaction.commit()


        }


    }


    private fun departmentAdapter() {


        val adapter: ArrayAdapter<String?> = object :
            ArrayAdapter<String?>(activity!!, android.R.layout.simple_spinner_dropdown_item) {
            override fun getView(
                position: Int,
                convertView: View?,
                parent: ViewGroup
            ): View {
                val v = super.getView(position, convertView, parent)
                if (position == count) {
                    (v.findViewById<View>(android.R.id.text1) as TextView).text = ""
                    (v.findViewById<View>(android.R.id.text1) as TextView).hint =
                        getItem(count) //"Hint to be displayed"
                }
                return v
            }

            override fun getCount(): Int {
                return super.getCount() - 1 // you dont display last item. It is used as hint.
            }
        }

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        for (department in d!!.toList()) {

            adapter.add(department)
        }

     /*   adapter.add("Two Days")
        adapter.add("Weekly")
        adapter.add("Monthly")
        adapter.add("Three Months")
        adapter.add("HINT_TEXT_HERE") //This is the text that will be displayed as hint.
*/

        deptTextView!!.setAdapter(adapter)

        deptTextView?.setSelection(0,0);
       // deptTextView!!.setSelection(adapter.count) //set the hint the default selection so it appears on launch.

       // deptTextView!!.setOnItemSelectedListener(this)












       /* val depts:ArrayList<String> = array!!
        for (department in array!!.toList()) {

            depts.add(department.toString())
        }
    // var arrayAdapter =DepartmentAdapter(activity, depts as java.util.ArrayList<String>?)
      *//*  var arrayAdapter = ArrayAdapter<String?>(activity!!.applicationContext, R.layout.layout,
                depts as List<String?>
            )*//*

        var arrayAdapter=DepartmentAdapter(activity!!,R.layout.layout,depts)
        deptTextView!!.setAdapter(arrayAdapter)*/
    }

    companion object {
        public var d =
            arrayOf("one", "two", "three", "four", "five")
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private const val ARG_PARAM1 = "param1"
        private const val ARG_PARAM2 = "param2"
        private const val ARG_array = "array"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment LoginFragment.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(
            param1: String?,
            param2: String?,
            arra: ArrayList<String>?
        ): LoginFragment {
            val fragment = LoginFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2 )
            args.putStringArrayList(ARG_array, arra!!)
            fragment.arguments = args
            fragment.array= arra!!
          // d= arra!!.toArray() as Array<String>
            staticarray=arra

            return fragment
        }
    }

}