package com.drinkw.gpa.fragment

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {

    //instance variable or globel
  public  var status=0;


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    // local variable
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        val fragment1 = LoginFragment()
        var array= ArrayList<String>()
        array.add("a")
        array.add("b")
        array.add("c")
        array.add("d")
        val fragment = LoginFragment.newInstance("simi","123456",array)
        fragmentTransaction.add(R.id.fra, fragment)
      //  fragmentTransaction.commit
        fragmentTransaction.addToBackStack("1")

        val fragment2 = ItemFragment.newInstance(3,"","")
        fragmentTransaction.add(R.id.fra2, fragment2)
        fragmentTransaction.commit()

       /* status
       var u= Utils()
        u.dep="Commerce"
        var u2= Utils()
        u.dep="Science"

       print(Utils.name) */

    }

    companion object
    {
        var x=0;
    }
}