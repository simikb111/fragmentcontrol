package com.drinkw.gpa.fragment.dummy;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.drinkw.gpa.fragment.R;

import java.util.ArrayList;
public  class DepartmentAdapter extends ArrayAdapter<String> {
    private Context context;
    private ArrayList<String> arrayList;
    private TextView dep;

    public DepartmentAdapter(@NonNull Context context, int resource, ArrayList<String> arrayList) {
        super(context, resource);
        this.context = context;
        this.arrayList = arrayList;
    }

    /* public DepartmentAdapter(Context context, ArrayList<String> arrayList) {
         super(context);
         this.context = context;
         this.arrayList = arrayList;
     }*/
    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.layout, parent, false);
        dep = convertView.findViewById(R.id.tv);

        return convertView;
    }
}